
Based on this Image you can create a ready-to-be-used owncloud-installation on your RaspberryPi (backed by a MySQL-Database). 

You can find the `Dockerfile`, all companion scripts as well as the `docker-compose.yml`-file in this [BitBucket-Repo][repo]

For a cut'n'paste able Screencast see also here:
[![asciicast](https://asciinema.org/a/18628.png?autoplay=1)](https://asciinema.org/a/18628?autoplay=1)

## Basic Setup

In order to keep things separated and to support a smooth update-process for future owncloud releases the whole installation consists of three different container.

	rpi-owncloud  ---+							// actual installation
	                 |
	                 +---> rpi-owncloud-db		// mysql database-backend
	                 |
	                 +---> rpi-owncloud-data	// volues for uploaded files
	
This installation uses a mysql database as backend. The database is encapsulated in a dedicated container (`rpi-owncloud-db`) which in turn is linked to the actual owncloud-container (`rpi-owncloud`). 

I also thought it would be benificial to separate the uploaded data as well as the configuration from the actual owncloud installation to make upgrades much easier. That's the reason for the `rpi-owncloud-data` container which just provides volumes to store these data.

## (Recommended) Usage

### Use docker-compose

To get all three container up and running with one command you should use `docker-compose`. Thanks to the guys from hypriot we have now also the possibility to use [that tool][docker-compose] on our RaspberryPi. 

So, install it, check out the repo and run `sudo docker-compose up` - that's it.

### Start everything by hand

So, how to get started. If you'd like to start everything manually you can do it this way:

#### start a MySQL-Instance

    	sudo docker run -d -p 3306:3306 --name owncloud-db \
    		-e OWNCLOUD_DB_USER=owncloud \
    		-e OWNCLOUD_DB_PASSWORD=mycloud \
    		-e MYSQL_ROOT_PASSWORD=foo \
    		schoeffm/rpi-owncloud-mysql

This will start a mysql deamonized container. In order to be useful for our purposes we have to provide a password for the root-user as well as a username and password for the technical owncloud user. The running container provides a `owncloud`-schema which is accessible by the given user. Next ...

#### create a data-volume

    	sudo docker run -d --name owncloud-data schoeffm/rpi-owncloud-data

After running this container it will immediately stop running since its only purpose is to provide a bunch of volumes for our actual owncloud-container - and for that it mustn't be running. As mentioned before, this should make updates of your owncloud-container much easier since you don't have to worry about your uploaded files or your `config.php`. Finally ...

#### combine everything

    	sudo docker run -d --name owncloud -p 80:80 -p 443:443 \
    		--link owncloud-db:mysql --volumes-from owncloud-data \ 
    		-e OWNCLOUD_SERVERNAME=your.domain.org schoeffm/rpi-owncloud

This is acutal owncloud installation which is accessible via port 80 and 443 (whereas 80 just redirects to 443 - we won't accept unsecured communication).

The overall setup is based on the very good tutorials of [Jan Karres][owncloud]. Some of the more important things include:

- [nginx][nginx_page] as web-server (smaller footprint, good performance)
- only secured communication over port SSL (port 80 will be redirected to 443)
- thus, on first start a self-signed certificate will be created (be sure you provide a valid `OWNCLOUD_SERVERNAME`)
- extended file-upload limits in your `php.ini` (2048M)
- preconfigured UTF-8 support
- ... _(see [here][nginx] and [here][owncloud] for more details)_


[repo]:https://bitbucket.org/schoeffm/rpi-docker/src/42b3fbcfe1b5743cd59b44905c6614b236c7e586/rpi-owncloud/?at=master
[docker-compose]:http://blog.hypriot.com/post/docker-compose-nodejs-haproxy/
[nginx]:http://jankarres.de/2012/08/raspberry-pi-webserver-nginx-installieren/
[owncloud]:http://jankarres.de/2013/10/raspberry-pi-owncloud-server-installieren/
[nginx_page]:http://nginx.com
